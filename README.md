Real-time applications:performance measurement based on time

cross-compiled with clang and tested on qemu-aarch64

clang --target=aarch64-linux-gnu -O2 *.c -o hashmap_clang64

qemu-aarch64 -L /home/kijin/projects/cpi/optee_repos/toolchains/aarch64/aarch64-linux-gnu/libc test_heatshrink_static
qemu-aarch64 -L /home/kijin/projects/cpi/optee_repos/toolchains/aarch64/aarch64-linux-gnu/libc ./heatshrink_clang64
qemu-aarch64 -L /home/kijin/projects/cpi/optee_repos/toolchains/aarch64/aarch64-linux-gnu/libc ./adpcm-xq_clang64 a2002011001-e02.wav afasf111211.wav

qemu-aarch64 -L /home/kijin/projects/cpi/optee_repos/toolchains/aarch64/aarch64-linux-gnu/libc ./blowpipe -E < cfe-5.0.1.src.tar.xz

